  ********** DockerFile ******

  Build Docker image,
  Apply security
  Determine single host or multi host networking
  Configure kubernetes docker deamon
  Configure and control docker using systemd
  Metrics with prometheus
  keep containers alive during deamnon downtime,
  Configure objects and apply metadata to objects
  Use Redis
  
  ***********  Dockerfile *******
  
  
  
  stages:
  - npm
  - code_quality
  - Dockerize
  - Deploy

variables:
  DOCKER_REGISTRY: registry.company.com
  GCR_DOCKER_REGISTRY: us.gcr.io
  Deployer_IMAGE: registry.company.com/tools/deploy:latest
  DOCKER_IMAGE: $DOCKER_REGISTRY/clt-test/mongotest
  GCR_DOCKER_IMAGE: $GCR_DOCKER_REGISTRY/test/mongotest
  GOOGLE_APPLICATION_CREDENTIALS: /tmp/key.json
  http_proxy: $PROXY
  https_proxy: $PROXY
  HTTP_PROXY: $PROXY
  HTTPS_PROXY: $PROXY
  
.npm_cache_template: &npm_cache
  cache:
    paths:
      - node_modules/

Install & Test:
  stage: npm
  image: registry.company.com/node:10.5.0
  script:
    - npm install
    - npm test
  coverage: /All files[^|]*\|[^|]*\s+([\d\.]+)/
  artifacts:
    paths:
      - coverage/
  <<: *npm_cache

include:
  - https:///gitlab.com/sonar-runner/raw/master/.gitlab-ci.scan.yml

code_quality:
  allow_failure: true
  except:
    - tags


Publish Docker Image to GCR:
  stage: Dockerize
  image: registry.company.com/gcloud:latest
  script:
    - echo ${GCP_TOOLS_SERVICE_ACCOUNT} > ${GOOGLE_APPLICATION_CREDENTIALS}
    - gcloud auth activate-service-account --key-file=${GOOGLE_APPLICATION_CREDENTIALS}
    - docker build -f Dockerfile --tag $GCR_DOCKER_IMAGE .
    - docker tag $GCR_DOCKER_IMAGE $GCR_DOCKER_IMAGE:latest
    - docker tag $GCR_DOCKER_IMAGE $GCR_DOCKER_IMAGE:$CI_COMMIT_SHA
    - docker images --all
    - gcloud auth configure-docker
    - docker push $GCR_DOCKER_IMAGE



Deploy [GKE - Tools Cluster]:
  stage: Deploy
  image: $DePLOYer_IMAGE
  script:
    - echo ${GCP_TOOLS_SERVICE_ACCOUNT} > ${GOOGLE_APPLICATION_CREDENTIALS}
    - dcploy deploy gke-dev --debug --image $GCR_DOCKER_IMAGE:$CI_COMMIT_SHA --no-smoketest



